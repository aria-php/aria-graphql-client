<?php

namespace ARIA\GraphQLClient\API\Fields;

trait GroupMembershipFields
{

  private $groupMembershipFields = '
    group,
    username
  ';
}
