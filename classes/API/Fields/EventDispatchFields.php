<?php

namespace ARIA\GraphQLClient\API\Fields;

trait EventDispatchFields
{

  private $eventDispatchFields = '
    success,
    message,
    context
  ';
}
