<?php

namespace ARIA\GraphQLClient\API\Fields;

trait GroupFields
{

  private $groupFields = '
    id,
    site_id,
    label,
    handler,
    options
  ';
}
