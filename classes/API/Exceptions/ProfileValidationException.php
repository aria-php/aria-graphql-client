<?php


namespace ARIA\GraphQLClient\API\Exceptions;

use RuntimeException;

class ProfileValidationException extends RuntimeException {}